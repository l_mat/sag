package pw.elka.flights;

/** Represents an entry in the flight timetable
 * A,B - flight relation
 * size - plane size 1..5 (small..dreamliner)
 */
public class Flight {
    private final String relation;
    private int size;
    protected long departure;
    protected long flightTime;
    protected double distance;

    public String getRelation() {
        return relation;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = Math.max(0,Math.min(5,size));
    }

    public long getDeparture() {
        return departure;
    }

    public void setDeparture(long departure) {
        this.departure = departure;
    }

    public long getFlightTime() {
        return flightTime;
    }

    public void setFlightTime(long flightTime) {
        this.flightTime = flightTime;
    }

    public double getDistance() {
        return distance;
    }

    public Flight(String relation, long time, int size, double distance) {
        this.relation = relation;
        this.departure = time;
        this.distance = distance;
        setSize(size);
    }
}
