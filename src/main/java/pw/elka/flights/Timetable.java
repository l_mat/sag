package pw.elka.flights;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;

// time in flight items is relative to 0 in ms
public class Timetable {
    FlightCalendar c = new FlightCalendar();
    final long period;

    class departureTimeComparator implements Comparator<Flight> {
        public int compare(Flight f1, Flight f2)
        {
            return (f1.getDeparture() > f2.getDeparture()) ? 1 : -1;
        }
    }

    TreeSet<Flight> table;
    WorldFacts metrics;

    Timetable(long period, WorldFacts metrics) {
        this.period = period;
        this.metrics = metrics;
        table = new TreeSet<>(new departureTimeComparator());
    }

    public void loadFile(String filename) {
        int n = 0;
        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
            String line;

            while ((line = br.readLine()) != null) {
                String[] values = line.split(" ");
                String relation = values[0];
                String[] time = values[2].split(":");
                Integer h = Integer.parseInt(time[0]);
                Integer m = Integer.parseInt(time[1]);
                Integer days = Integer.parseInt(values[1], 2);

                for(int d = 0; d < 7; d++) {
                    if((days & (1 << (6 - d))) > 0) {
                        int timeRel = 1000 * (0 + 60 * (m + 60 * (h + 24 * d)));
                        add(relation, timeRel);
                    }
                }
                n++;
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        System.out.println("Loaded timetable "+ filename + ", n = " + n);
    }

    public void add(String flightNo, long time) {
        double dist = metrics.getDist(flightNo);
        table.add(new Flight(flightNo, time, 3, dist));
    }
}