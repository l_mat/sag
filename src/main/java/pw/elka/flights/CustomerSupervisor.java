package pw.elka.flights;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/** akka agent represeting customer population
 */
public class CustomerSupervisor extends AbstractActor {
    static public Props props(ActorSystem system, ActorRef brokerSupervisor, int numChildren, WorldFacts wf) {
        return Props.create(CustomerSupervisor.class, () -> new CustomerSupervisor(system, brokerSupervisor, numChildren, wf));
    }

    static public class UpdateBrokerList {
        public final String who;

        public UpdateBrokerList(String who) {
            this.who = who;
        }
    }

    static  public class msgConn {
        public final List<String> conn;

        public msgConn(List<String> conn) {
            this.conn = conn;
        }
    }

    private final ActorRef brokerSupervisor;
    private final ArrayList<ActorRef> children;

    public CustomerSupervisor(ActorSystem system, ActorRef brokerSupervisor, int numChildren, WorldFacts wf) {
        this.brokerSupervisor = brokerSupervisor;
        this.children = new ArrayList<>();

        codes = new LinkedList<>();
        codes.addAll(wf.getCodes());

        brokerSupervisor.tell("brokerList", getSelf());
        /*
        brokerSupervisor.tell("brokerList",getSelf());
        Timeout timeout = new Timeout(5,TimeUnit.SECONDS);
        Future<Object> future = Patterns.ask(brokerSupervisor,"brokerList", timeout);
        */
        try {
            /*
            ArrayList<ActorRef> result = (ArrayList<ActorRef>) Await.result(future, timeout.duration());
            System.out.println("otrzymano " + result.size());
            */

            for (int i = 0; i < numChildren; ++i) {
                int cn = getCustomerNumber();
                ActorRef actor = context().actorOf(Customer.props(context().parent(), cn), "customer" + cn);
                // tell the customer that the system is up and running
                //actor.tell("init", ActorRef.noSender());
                children.add(actor);
            }
        }
        catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    @Override
    public void preStart() {

    }

    // override postRestart so we don't call preStart and schedule a new message
    @Override
    public void postRestart(Throwable reason) {
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .matchEquals("restart", message -> {
                    throw new ArithmeticException();
                })
                .match(msgConn.class, message -> {
                    // propagate connectivity list
                    context().actorSelection("../customerSupervisor/*").forward(message, getContext());
                })
                .match(SystemSupervisor.ActorList.class, message -> {
                    context().parent().tell(new SystemSupervisor.msgLog("CS: Received actor list of "+message.list.size()),getSelf());
                    int i = 0;
                    for(ActorRef actor : children)
                        actor.tell(new Customer.msgInit(message.list.get(i++ % message.list.size()), codes), ActorRef.noSender());
                })
                .build();
    }

    private int getCustomerNumber() {
        return customerNumber++;
    }
    private int customerNumber = 0;
    private List<String> codes;
}