package pw.elka.flights;

import java.text.SimpleDateFormat;
import java.util.Date;

/** FlightCalendar
 *  represents a calendar for the flight simulation system
 *  time from t0 runs at the speed of invScale
 */
public class FlightCalendar {
    static private final long t0 = System.currentTimeMillis();
    static private final long tOffs = 1559512800000L; // 3.06.2019 - poniedziałek
    //static private final long invScale = 36000;
    static private final long invScale = 3600;

    static SimpleDateFormat dt = new SimpleDateFormat("E yyyy-MM-dd HH:mm:ss");

    FlightCalendar() {

    }

    public Date asDate() {
        long t = getTime();
        Date d = new Date(invScale * t + tOffs);
        return d;
    }
    public Date toDate(long t) {
        Date d = new Date(invScale * t + tOffs);
        return d;
    }
    static public String getString(long t) {
        Date d = new Date(invScale * t + tOffs);
        return dt.format(d);
    }

    static public long getCurrentWeek() { return getTime() / getMsPerWeek(); }

    static public long getMsPerWeek() {
        return  (1000 * 60 * 60 * 24 * 7) / invScale;
    }

    static public long stretchTime(long t) {
        return t / invScale;
    }

    public long dateRelative(int d, int h, int m) {
        return ( 1000 * (0 + (60 * (m + 60 * (h + (24 * d)))))) / invScale;
    }

    static public long getBoardingTime() {
        return 1000 * 60 * 60 * 2;
    }
    static public long getTransferTime() {
        return 1000 * 60 * 60 * 1;
    }

    public static long getTime() {
        return System.currentTimeMillis() - t0;
    }
}
