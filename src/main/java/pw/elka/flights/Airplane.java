package pw.elka.flights;

import com.sun.deploy.util.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/** Represents a scheduled flight for which tickets are being sold
 * departure - the object is to be used until departure time, then serialized to a log
 */
public class Airplane extends Flight{
    double flightSpeed = 800.0; // km/h
    double fuelPerKm = 12.0; // l/km

    static final int seatsPerRow = 6;
    private String[] seats;
    private int freeSeats;

    public int getnSeats() {
        return nSeats;
    }
    // fuel usage [l]
    public double getFuelUsage() { return distance * fuelPerKm; }
    // travel time [h]
    public long getTravelTime() { return (long)((distance / (double)flightSpeed) * 60 * 60 * 1000); }

    private final int nSeats;

    private final long arrival;
    private int size;

    public Integer getFlightNo() {
        return flightNo;
    }

    private Integer flightNo;

    public Airplane(Flight flight, long departure, Integer flightNo) {
        super(flight.getRelation(), departure, flight.getSize(),flight.getDistance());
        this.flightNo = flightNo;
        this.arrival = this.departure + FlightCalendar.stretchTime(getTravelTime());

        int rows = (flight.getSize() + 3) * 6;
        this.nSeats = rows * Airplane.seatsPerRow;
        this.freeSeats = this.nSeats;
        this.seats = new String[this.nSeats];
        Arrays.fill(seats,"");
    }

    public int getFreeSeats() {
        return freeSeats;
    }
    // placeholder
    public long getArrival() {return  this.arrival; }

    // result - success?
    public synchronized boolean putTicket(Airline.airlineOffer offer) {
        Boolean stillFree = true;

        for(Integer i:offer.seatIndices) {
            if(seats[i].length() > 0) {
                stillFree = false;
            }
        }
        if (stillFree) {
            freeSeats -= offer.seatIndices.length;
            for(Integer i:offer.seatIndices) {
                seats[i] += (offer.orderNo); // += to test overbooking possibility
            }
        }
        return stillFree;
    }

    private int tabu = 0; // for simple seat allocation control

    public synchronized Optional<Integer[]> assignSeats(int n, String orderNo) {
        Integer[] result = new Integer[n];
        Arrays.fill(result, -1);
        int j = 0;
        if(n <= freeSeats) {
            for(int ii = 0; ii < nSeats && j<n; ++ii) {
                int i = (ii + tabu) % nSeats;
                if(seats[i].length() == 0) {
                    result[j] = i;
                    j++;
                }
            }
        }
        if(j != n || n == 0) {
            return Optional.empty();
        }
        else {
            tabu = (tabu+n) % nSeats;
            return Optional.of(result);
        }
    }

    public String toString() {
        List<String> ss = new ArrayList<>();
        for(int i = 0; i<nSeats;i++)
            ss.add(seats[i].toString());
        return "{"+StringUtils.join(ss,"; ")+"}";
    }
}
