package pw.elka.flights;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;

import java.util.*;


public class AirlineSupervisor extends AbstractActor {

    static public Props props(ActorSystem system, ActorRef printerActor, String [] names) {
        return Props.create(AirlineSupervisor.class, () -> new AirlineSupervisor(system, printerActor, names));
    }

    private final ActorRef airlineSupervisor;
    private ArrayList<ActorRef> children;
    private WorldFacts wf;

    public AirlineSupervisor(ActorSystem system, ActorRef airlineSupervisor, String [] names) {
        this.airlineSupervisor = airlineSupervisor;
        this.wf = new WorldFacts("coordinates.txt");

        this.children = new ArrayList<>();
        this.connectivity = new HashMap<>();
        this.conn = new LinkedList<>();

        for(String name : names) {
            ActorRef actor = context().actorOf(Airline.props(getContext().parent(), name+".txt",wf), name);
            actor.tell("init", ActorRef.noSender());
            children.add(actor);
        }
    }

    @Override
    public void preStart() {

    }

    // override postRestart so we don't call preStart and schedule a new message
    @Override
    public void postRestart(Throwable reason) {
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(Airline.Connectivity.class, message -> {
                    connectivity.put(sender().path().name(), message);
                    // update whole connectivity info
                    List<String> newConn = new LinkedList<>();
                    for(String a : connectivity.keySet()) {
                        for(String s:connectivity.get(a).c.keySet()) {
                            newConn.add(s);
                        }
                    }
                    conn = newConn;
                    context().actorSelection("akka://akka/user/systemSupervisor/customerSupervisor").tell(
                            new CustomerSupervisor.msgConn(conn),
                            getSelf()
                    );
                    // updated connectivity
                    System.out.println("AS: updated connectivity "+ conn.toString());
                })
                .matchEquals("airlineList", message -> {
                    context().parent().tell(new SystemSupervisor.msgLog("requested airline list"), getSelf());
                    sender().tell(new SystemSupervisor.ActorList(children), getSelf());
                })
                .matchEquals("restart", message -> {
                    throw new ArithmeticException();
                })
                .build();
    }

    Map<String,Airline.Connectivity> connectivity;
    List<String> conn;
}
