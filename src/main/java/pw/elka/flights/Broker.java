package pw.elka.flights;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.japi.Pair;
import scala.concurrent.duration.Duration;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.TimeUnit;

public class Broker extends AbstractActor {

    static public Props props(ActorRef printerActor) {
        return Props.create(Broker.class, () -> new Broker(printerActor));
    }

    static public class msgSearch {
        public final String orderNo, relation;
        public final int seats;
        public final long timeMin, timeMax;
        //public final double priceMax;
        public final ActorRef customer;
        public String airline;
        public long flightNo;
        public long validTo;
        public final String searchMode;

        public String getRelation() {return relation; };

        public msgSearch(String orderNo, String relation, int seats, String searchMode, ActorRef customer) {
            this.orderNo = orderNo;
            this.relation = relation;
            this.customer = customer;
            this.searchMode = searchMode;
            this.seats = seats;
            //this.priceMax = 1000.0;
            this.timeMin = 0;
            this.timeMax = Long.MAX_VALUE;
        }
    }

    static public class msgSelectOffer {
        public final msgSearch search;

        public msgSelectOffer(msgSearch search) {
            this.search = search;
        }
    }

    // represents reservation with payment if accepted
    static public class msgPayment {
        public final String orderNo;
        public final Integer flightNo;

        public msgPayment(String orderNo, Integer flightNo) {
            this.orderNo = orderNo;
            this.flightNo = flightNo;
        }
    }

    private ArrayList<ActorRef> myAirlines;
    static public class msgInit {
        public final ArrayList<ActorRef> airlines;
        public msgInit(ArrayList<ActorRef> airlines) {
            this.airlines = airlines;
        }
    }

    private final ActorRef printerActor;

    public Broker(ActorRef printerActor) {
        this.printerActor = printerActor;
        this.offerInbox = new ConcurrentSkipListMap<>();
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(msgInit.class, message -> { this.myAirlines = message.airlines; } )
                .match(msgSearch.class, message -> {
                    printerActor.tell(new SystemSupervisor.msgLog(getSelf().path().name() + " is searching for " +
                            message.relation + " for " + sender().path().name()), getSelf());
                    // TODO different search modes
                    if(!message.searchMode.equals("first")) {
                        printerActor.tell(new SystemSupervisor.msgLog(getSelf().path().name() +
                                " search mode not supported: " + message.searchMode), getSelf());
                    } else {
                        // check airline schedules
                        for (ActorRef airline : myAirlines) {
                            // ask the airline for a  connection
                            // TODO joined flights
                            printerActor.tell(new SystemSupervisor.msgLog(getSelf().path().name() +
                                    " asking an airline " + airline.path().name()), getSelf());
                            message.validTo = FlightCalendar.getTime() + 200;
                            airline.tell(message, getSelf());
                        }

                        if (!offerInbox.containsKey(message.orderNo)) // first offer
                            offerInbox.put(message.orderNo, new Pair(message,new ArrayList<>()));
                        // after a delay, check the offers and
                        getContext().getSystem().scheduler().scheduleOnce(
                                Duration.create(10, TimeUnit.MILLISECONDS),
                                getSelf(), new msgSelectOffer(message), getContext().dispatcher(), null);
                    }
                })
                .match(Airline.tryPayment.class, message -> {
                    // TODO resend the order if automatic seat selection
                    printerActor.tell(new SystemSupervisor.msgLog(getSelf().path().name() + " RETRY AT " + sender().path().name()), getSelf());

                    if(offerInbox.containsKey(message.offer.orderNo)) {
                        msgSearch search = offerInbox.get(message.offer.orderNo).first();
                        getContext().getSystem().scheduler().scheduleOnce(
                                Duration.create(1, TimeUnit.MILLISECONDS),
                                getSelf(), search, getContext().dispatcher(), null);
                    }
                })
                .match(msgSelectOffer.class, message -> {
                    // select an offer from the airlines
                    String n = message.search.orderNo;
                    ActorRef c = message.search.customer;
                    // todo update conditions and messages
                    if(offerInbox.containsKey(n)) { // select one offer to buy for the client
                        printerActor.tell(new SystemSupervisor.msgLog(getSelf().path().name() + " select out of " +
                                offerInbox.get(n).second().size()), getSelf());
                        // PLACEHOLDER: just select the first incoming offer
                        if(offerInbox.get(n).second().size() > 0) {
                            Airline.airlineOffer offer = offerInbox.get(n).second().get(0);
                            // c.tell(offer, getSelf());
                            printerActor.tell(new SystemSupervisor.msgLog(getSelf().path().name() + " selected - trying payment " +
                                    offer.airline.path().name() + " " + offer.relation + " " + offer.flightNo), getSelf());
                            offer.airline.tell(new Airline.tryPayment(offer), getSelf());
                        } else
                            printerActor.tell(new SystemSupervisor.msgLog("Order #"+n+" - empty offer!"), getSelf());
                    } else { // no relevant offers in the inbox
                        printerActor.tell(new SystemSupervisor.msgLog("Order #"+n+" - no offers arrived on time!"), getSelf());
                        // tell the customer there is no hope
                        c.tell(Airline.airlineOffer.failure(n, message.search.customer),getSelf());
                    }
                    // remove offers for the processed order
                    offerInbox.remove(n);
                })
                .match(Airline.airlineOffer.class, message -> {
                    printerActor.tell(new SystemSupervisor.msgLog(getSelf().path().name() +
                            " received an offer for order "+ message.orderNo+": "+message.relation + ",  "+
                            message.seatIndices.length+" seats ,  from " + sender().path().name()), getSelf());
                    if(message.flightNo == -2) { // may retry
                        printerActor.tell(new SystemSupervisor.msgLog(getSelf().path().name() +
                                " airline suggested to retry " + message.orderNo + ", " +
                                message.relation+" via " + sender().path().name()), getSelf());
                    } else
                    if(message.validTo == -1) {
                        message.customer.tell(message,getSelf());
                    } else
                    if(message.validTo >= FlightCalendar.getTime()) {

                        offerInbox.get(message.orderNo).second().add(message);
                    }
                })
                .match(Airline.msgConfirmPayment.class, message -> {
                    message.offer.customer.tell(message, getSelf());
                })
                .matchEquals("restart", message -> {
                    throw new ArithmeticException();
                })
                .build();
    }

    private SortedMap<String, Pair<msgSearch, List<Airline.airlineOffer>>> offerInbox;
}
