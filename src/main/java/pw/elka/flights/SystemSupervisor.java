package pw.elka.flights;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.event.Logging;
import akka.event.LoggingAdapter;

import java.util.ArrayList;

public class SystemSupervisor extends AbstractActor {
    // AKKA
    static public Props props(ActorSystem system) {
        return Props.create(SystemSupervisor.class, () -> new SystemSupervisor(system));
    }

    // AKKA messages
    static public class msgLog {
        public final String message;

        public msgLog(String message) {
            this.message = message;
        }
    }

    static public class ActorList {
        public final ArrayList<ActorRef> list;

        public ActorList(ArrayList<ActorRef> list) {
            this.list = list;
        }
    }
    // <<

    private LoggingAdapter log;

    private final ActorSystem system;

    public SystemSupervisor(ActorSystem system) {
        this.system = system;

        log = Logging.getLogger(getContext().getSystem(), this);
    }

    static String [] airlineNames = {"LOT", "DLH", "KLM"};

    public void preStart() {
        final ActorRef airlineSupervisor =
                context().actorOf(AirlineSupervisor.props(system, getSelf(), airlineNames), "airlineSupervisor");
        final ActorRef brokerSupervisor =
                context().actorOf(BrokerSupervisor.props(system, airlineSupervisor, 2), "brokerSupervisor");
        final ActorRef customerSupervisor =
                context().actorOf(CustomerSupervisor.props(system, brokerSupervisor,100, worldFacts), "customerSupervisor");
    }

    // override postRestart so we don't call preStart and schedule a new message
    @Override
    public void postRestart(Throwable reason) {
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(msgLog.class, m -> {
                    log.info(m.message);
                })
                .matchAny(msg ->
                        log.warning("Received unknown message: {}", msg)
                )
                .build();
    }

    WorldFacts worldFacts = new WorldFacts("distances.txt");
}
