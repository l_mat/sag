package pw.elka.flights;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;

import static akka.pattern.Patterns.ask;
import static akka.pattern.Patterns.pipe;

import java.util.ArrayList;

public class BrokerSupervisor extends AbstractActor {

    static public Props props(ActorSystem system, ActorRef printerActor, int numChildren) {
        return Props.create(BrokerSupervisor.class, () -> new BrokerSupervisor(system, printerActor, numChildren));
    }

    static public class UpdateAirlineList {
        public UpdateAirlineList() {
        }
    }

    private final ActorRef airlineSupervisor;
    private ArrayList<ActorRef> children;

    public BrokerSupervisor(ActorSystem system, ActorRef airlineSupervisor, int numChildren) {
        this.airlineSupervisor = airlineSupervisor;

        this.children = new ArrayList<>();

        airlineSupervisor.tell("airlineList", getSelf());

        for(int i = 0; i < numChildren; ++i) {
            ActorRef actor = context().actorOf(Broker.props(context().parent()), "broker"+i);
            actor.tell("init", ActorRef.noSender());
            children.add(actor);
        }
    }

    @Override
    public void preStart() {

    }

    // override postRestart so we don't call preStart and schedule a new message
    @Override
    public void postRestart(Throwable reason) {
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(SystemSupervisor.ActorList.class, message -> {
                    context().parent().tell(
                            new SystemSupervisor.msgLog("BS: Received actor list of "+message.list.size()),getSelf());

                    for(ActorRef actor : children)
                        actor.tell(new Broker.msgInit(message.list), ActorRef.noSender());
                })
                .matchEquals("brokerList", message -> {
                    context().parent().tell(new SystemSupervisor.msgLog("requested broker list"), getSelf());
                    sender().tell(new SystemSupervisor.ActorList(children), getSelf());
                })
                .matchEquals("restart", message -> {
                    throw new ArithmeticException();
                })
                .build();
    }
}
