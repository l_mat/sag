package pw.elka.flights;

import java.io.IOException;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;

public class AkkaSystem {
  public static void main(String[] args) {
    final ActorSystem system = ActorSystem.create("akka");
    try {

      final ActorRef systemSupervisor =
              system.actorOf(SystemSupervisor.props(system), "systemSupervisor");

      System.out.println(">>> Press ENTER to exit <<<");
      System.in.read();
    } catch (IOException ioe) {
    } finally {
      system.terminate();
    }
  }
}
