package pw.elka.flights;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Cancellable;
import akka.actor.Props;

import java.util.*;
import java.time.Duration;
import java.util.concurrent.ConcurrentSkipListMap;

/** Represents an akka Airline agent
 */
public class Airline extends AbstractActor {
    FlightCalendar cal = new FlightCalendar();

    // AKKA props
    static public Props props(ActorRef printerActor, String name,WorldFacts metrics) {
        return Props.create(Airline.class, () -> new Airline( printerActor, name,metrics));
    }
    // AKKA messages
    static public class airlineOffer {
        public static airlineOffer failure(String orderNo, ActorRef customer) {
            return new airlineOffer(orderNo,"",-1,-1,-1,customer,-1,FlightCalendar.getTime()+10000,ActorRef.noSender(),new Integer[1]);
        }
        public static airlineOffer mayRetry(String orderNo, ActorRef customer) {
            return new airlineOffer(orderNo,"",-1,-1,-1,customer,-2,FlightCalendar.getTime()+10000,ActorRef.noSender(),new Integer[1]);
        }
        public airlineOffer(String orderNo, String relation, long departure, long arrival, int price, ActorRef customer, Integer flightNo,long validTo, ActorRef airline,Integer[]seatIndices) {
            this.orderNo = orderNo;
            this.relation = relation;
            this.departure = departure;
            this.arrival = arrival;
            this.price = price;
            this.customer = customer;
            this.flightNo = flightNo;
            this.validTo = validTo;
            this.airline = airline;
            this.seatIndices = seatIndices;
        }

        public final Integer [] seatIndices;
        public final String orderNo, relation;
        public final long departure, arrival;
        public final int price;
        public final ActorRef customer;
        public final Integer flightNo;
        public final long validTo;
        public final ActorRef airline;
    }

    static public class Connectivity {
        Connectivity(Map<String,Integer> c) {
            this.c = c;
        }
        Map<String,Integer> c;
    }

    static public class tryPayment {
        public tryPayment(airlineOffer offer) {
            this.offer = offer;
        }

        public final airlineOffer offer;
    }

    static public class msgConfirmPayment {
        public msgConfirmPayment(airlineOffer offer) {
            this.offer = offer;
        }

        public final airlineOffer offer;
    }

    // << AKKA
    //Deque<Airplane> runway;
    Deque<Flight> timetable;
    Timetable currentTimetable;

    class flightNoComparator implements Comparator<Airplane> {
        public int compare(Airplane f1, Airplane f2)
        {
            return (f1.getFlightNo() > f2.getFlightNo()) ? 1 : -1;
        }
    }

    private SortedMap<Integer,Airplane> runway;

    private Integer flightCounter = 0;
    private Integer nextAirplaneNumber() {
        return flightCounter++;
    }

    private long timeTablePeriod;
    // last epoch with transcribed timetable
    private long timeTableEpoch;
    // number of epochs (current included) for which tickets are sold
    private final long sellEpochs = 3;

    private final ActorRef printerActor;
    private Cancellable tick;
    private WorldFacts metrics;

    public Airline(ActorRef printerActor, String scheduleFile, WorldFacts metrics) {
        this.metrics = metrics;
        this.timeTableEpoch = FlightCalendar.getCurrentWeek() - 1;
        this.printerActor = printerActor;
        this.runway = new ConcurrentSkipListMap<>();
        this.timetable = new LinkedList<>();
        this.timeTablePeriod = cal.getMsPerWeek();
        this.currentTimetable = new Timetable(timeTablePeriod, metrics);
        currentTimetable.loadFile(scheduleFile);

        // periodic updates and reports
        tick = getContext().getSystem().scheduler().schedule(
                Duration.ZERO,
                Duration.ofMillis(1500),
                getSelf(), "tick", getContext().dispatcher(), null);
    }

    public synchronized void reportFlights() {
        long tDep = FlightCalendar.getTime();

        while(!runway.isEmpty()) {
            Airplane flight = runway.get(runway.firstKey());
            if(flight.getDeparture() <= tDep) {
                printerActor.tell(new SystemSupervisor.msgLog(getSelf().path().name() +
                        "0 DEPARTURE: "+ flight.getFlightNo()+": "+ flight.getRelation() +
                        " " + FlightCalendar.getString(flight.getDeparture()) +
                        " to arrive: " + FlightCalendar.getString(flight.getArrival()) +
                        " " + flight.getFreeSeats() + "/" + flight.getnSeats() + "\n "+flight.toString()), getSelf());
                runway.remove(runway.firstKey());
            } else // no more departed flights to report
                break;
        }
        // remove and report flight that have departed
        //Predicate<Airplane> p = (a) -> a.getDeparture() <= tDep;
        //runway.removeIf(p);
    }

    public synchronized void scheduleFlight(Airplane item) {
        // only after the already scheduled flights
        /*if(runway.size() > 0) {
            if(runway.last().getDeparture() <= item.getDeparture()) {
                runway.add(item);
            } else {
                throw new RuntimeException("Not yet implemented: insert to schedule.");
            }
        } else runway.add(item);
        */
        runway.put(item.getFlightNo(),item);
    }

    public void preStart() {

    }
    // override postRestart so we don't call preStart and schedule a new name
    @Override
    public void postRestart(Throwable reason) {
    }

    private void sendTick() {
        getContext().getSystem().scheduler().scheduleOnce(
                Duration.ofMillis(1500),
                getSelf(), "tick", getContext().dispatcher(), null);
    }

    @Override
    public void postStop() {
        tick.cancel();
    }

    // TODO synchronized???
    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(Broker.msgSearch.class, message -> {
                    // TODO price calculation
                    printerActor.tell(new SystemSupervisor.msgLog(getSelf().path().name() +
                            " was asked for "+message.relation + " for " + sender().path().name()), getSelf());
                    // TODO check if there is an offer for the requested connection
                    // TODO placeholder:

                    Optional<Airplane> a = findFlight(message);
                    if(a.isPresent()) {
                        Airplane airplane = a.get();
                        Optional<Integer []> ss = airplane.assignSeats(message.seats,message.orderNo);
                        if(!ss.isPresent()) {
                            printerActor.tell(new SystemSupervisor.msgLog(getSelf().path().name() + " error assigning seats"), getSelf());
                        } else {
                            airlineOffer offer = new airlineOffer(
                                    message.orderNo, message.relation, airplane.getDeparture(), airplane.getArrival(),
                                    100, message.customer, airplane.getFlightNo(), FlightCalendar.getTime() + 500,
                                    getSelf(), ss.get());
                            sender().tell(offer, getSelf());
                        }
                    } // else no offer to send
                })
                .match(Airline.tryPayment.class, message -> {
                    Airline.airlineOffer o = message.offer;
                    printerActor.tell(new SystemSupervisor.msgLog(getSelf().path().name() +
                            " try payment "+ o.orderNo + "/" + o.flightNo), getSelf());

                    if(runway.containsKey(o.flightNo)) {
                        // put tickets
                        Airplane a = runway.get(o.flightNo);
                        // TODO try to place tickets on the places from offer

                            if (a.putTicket(o)) {
                                printerActor.tell(new SystemSupervisor.msgLog(getSelf().path().name() +
                                        " received payment for order " + o.orderNo + "/" + o.flightNo), getSelf());
                                sender().tell(new msgConfirmPayment(o),getSelf());
                            } else {
                                printerActor.tell(new SystemSupervisor.msgLog(getSelf().path().name() +
                                        " OFFER EXPIRED (someone else payed first?) " + o.orderNo + "/" + o.flightNo), getSelf());
                                // there are still places in the plane, maybe choose other seats
                                if(a.getFreeSeats() >= o.seatIndices.length)
                                    sender().tell(airlineOffer.mayRetry(o.orderNo, o.customer), getSelf());
                                sender().tell(message,getSelf());
                            }

                    } else {
                       printerActor.tell(new SystemSupervisor.msgLog(getSelf().path().name() +
                               " PAYMENT CANCELLED: FLIGHT NOT FOUND (departed?) " + o.orderNo + "/" + o.flightNo), getSelf());
                       sender().tell(airlineOffer.failure(o.orderNo,o.customer), getSelf());
                    }
                })
                .matchEquals("restart", message -> {
                    throw new ArithmeticException();
                })
                .matchEquals("tick", message -> {
                    reportFlights();
                    transcribeTimetable();
                })
                .build();
    }

    public synchronized void transcribeTimetable() {
        long t = FlightCalendar.getTime();
        // d - timetable transcription alert time, d > tick interval
        long d = 10 * boardingTime;

        // timeTableEpoch - at least current + sellEpoch - 1
        long currentEpoch = t / timeTablePeriod;
        long alert = (t+d) / timeTablePeriod;

        if(currentEpoch != alert)
            printerActor.tell(new SystemSupervisor.msgLog(getSelf().path().name() +
                    " NEXT EPOCH " + currentEpoch + " => " + alert), getSelf());

        // complete runway --- "while" only for the sake of initialization for as much periods as needed
        Boolean change = false;
        while(timeTableEpoch < (((t+d) / timeTablePeriod) + sellEpochs - 1)) {
            // transcribe currentTimetable for the following period
            timeTableEpoch++;
            change = true;
            transcribeTimetableEpoch(timeTableEpoch);
        }
        // report connections to parent
        if(change)
            reportConnectivity();
    }

    // todo handle timetable changes
    public void reportConnectivity() {
        Map<String, Integer> connectivity = new HashMap<>();

        for (Flight item : currentTimetable.table) {
            if (!connectivity.containsKey(item.getRelation()))
                connectivity.put(item.getRelation(), 1);
            else
                connectivity.put(item.getRelation(), connectivity.get(item.getRelation()) + 1);
        }
        getContext().parent().tell(new Connectivity(connectivity), getSelf());
    }

    public void transcribeTimetableEpoch(long epoch) {
        printerActor.tell(new SystemSupervisor.msgLog(getSelf().path().name() +
                " TRANSCRIBE " + epoch+ " of " + this.timeTableEpoch), getSelf());
        long t0 = epoch * timeTablePeriod;
        long now = FlightCalendar.getTime();

        for(Flight item : currentTimetable.table) {
            long t = t0 + FlightCalendar.stretchTime(item.getDeparture());
            if(t > now + boardingTime)
                scheduleFlight(new Airplane(new Flight(item.getRelation(), t,1, item.getDistance()),t, nextAirplaneNumber()));
        }
    }

    // todo list of flights
    public Optional<Airplane> findFlight(Broker.msgSearch m) {
        Iterator<Map.Entry<Integer, Airplane>> it = runway.entrySet().iterator();

        while(it.hasNext()) {
            Map.Entry <Integer, Airplane> e = it.next();

            Airplane a = e.getValue();
            if(a.getDeparture() > m.timeMax)
                break; // too late

            if(a.getDeparture() < m.timeMin || a.getDeparture() < FlightCalendar.getTime() + boardingTime) // too early
                continue;

            if(a.getRelation().equals(m.getRelation()))
                if(a.getFreeSeats() >= m.seats)
                    return Optional.of(a);
        }
        return Optional.empty();
    }

    long boardingTime = 200;
}
