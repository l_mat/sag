package pw.elka.flights;

public class Ticket {
    private static FlightCalendar cal = new FlightCalendar();
    public static final long ticketReservationTime = 10;

    public Ticket() {
        this.reservationTo = -1;
        this.orderNo = "";
    }

    public String getOrderNo() {
        return orderNo;
    }

    // reservation
    public void setOrderNo(String orderNo) {

            this.orderNo = orderNo;

    }

    // orderNo          ""          [present]   [present]
    // reservationTo    -1          -1          [time]
    //                  empty       payed       reserved
    // toString         ""          [orderNo]   ""
    private String orderNo;
    private long reservationTo;

    public void checkReservation() {

        if(reservationTo != -1 && (cal.getTime() > this.reservationTo)) {
            // reservation expired
            this.orderNo = "";
            this.reservationTo = -1;
        }
    }

    public void payment(String orderNo) {
        this.orderNo = orderNo;
        /*
        if(orderNo == this.orderNo) {
            this.reservationTo = -1;
        } else {
            System.out.println("UNEXPECTED");
        }*/
    }

    Boolean isEmpty() {
        //if(reservationTo != -1)
        //    checkReservation();
        return orderNo.isEmpty();
    }

    public String toString() {
        if(orderNo.length()>0) {
            return orderNo;
        } else return "FREE";
    }

    public void cancel() {
        this.orderNo = "";
        this.reservationTo = -1;
    }

}