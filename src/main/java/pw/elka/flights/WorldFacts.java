package pw.elka.flights;


import akka.japi.Pair;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;

public class WorldFacts {
    static Map<String,Double> dist = new HashMap<>();
    static Map<String, Pair<Integer,Integer>> coord = new HashMap<>();

    public WorldFacts(String filename) {
        readCoordinates(filename);
    }

    public double getDist(String relation) {
        String[] res = relation.split("[\\p{Punct}\\s]+");
        double sum = 0.0;
        if(res.length >= 2) {
            for(int i = 0; i < res.length - 1; i++)
                if(coord.containsKey(res[i]) && coord.containsKey(res[i + 1])) {
                    Pair<Integer,Integer>a = coord.get(res[i]);
                    Pair<Integer,Integer>b = coord.get(res[i + 1]);
                    sum += Math.sqrt(Math.pow(a.first()-b.first(), 2) + Math.pow(a.second()-b.second(), 2) );
                } else
                    throw new IndexOutOfBoundsException("Incorrect airport code! " + relation);
        } else
        throw new IndexOutOfBoundsException("Incorrect argument format! " + relation);
        return sum;
    }

    private void readCoordinates(String filename) {
        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(" ");
                String code = values[0];
                Integer x = Integer.parseInt(values[1]) * 100;
                Integer y = Integer.parseInt(values[2]) * 100;
                coord.put(code, new Pair<>(x,y));
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        System.out.println("Loaded "+coord.size() + " airport coordinates.");
    }

    public Set<String> getCodes() { return coord.keySet(); }

    private boolean checkCode(String code) {
        return coord.containsKey(code);
    }
}
