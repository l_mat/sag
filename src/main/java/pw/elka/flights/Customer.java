package pw.elka.flights;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Cancellable;
import akka.actor.Props;
import com.sun.org.apache.xpath.internal.operations.Bool;
import scala.concurrent.duration.Duration;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class Customer extends AbstractActor {

    static public Props props(ActorRef printerActor, int tickIntervalSeed) {
        return Props.create(Customer.class, () -> new Customer(printerActor, tickIntervalSeed));
    }

    // scheduling periodic messages
    // https://doc.akka.io/docs/akka/2.5.3/java/howto.html
    /*
    private final Cancellable tick = getContext().getSystem().scheduler().schedule(
          Duration.create(1500, TimeUnit.MILLISECONDS),
          Duration.create(1, TimeUnit.SECONDS),
          getSelf(), "tick", getContext().dispatcher(), null);
    */
    @Override
    public void postStop() {
        tick.cancel();
    }
    Cancellable tick;

    // TODO handling dead broker
    private ActorRef myBroker;
    static public class msgInit {
        public final ActorRef broker;
        public final List<String> codes;
        public msgInit(ActorRef broker,List<String> codes) {
            this.broker = broker;
            this.codes = codes;
        }
    }

    private final ActorRef printerActor;
    private String greeting = "";

    int orderCounter;
    static Random rnd, rndRelation, rndMode;

    public Customer(ActorRef printerActor, int tickIntervalSeed) {
        this.printerActor = printerActor;
        this.orderCounter = 0;
        myOrders = new HashMap<>();
        //backlog = new TreeMap<>();
        rnd = new Random(tickIntervalSeed);
        rndRelation = new Random(tickIntervalSeed * 13);
        rndMode = new Random(tickIntervalSeed * 11);
        conn = new LinkedList<>();
    }

    @Override
    public void preStart() {

    }

    // override postRestart so we don't call preStart and schedule a new message
    @Override
    public void postRestart(Throwable reason) {
    }

    @Override
    public Receive createReceive() {
    return receiveBuilder()
            // customer receives an initializing message with a pre-assigned broker reference
            // TODO what to do when the selected broker is down - wait or request another broker handle
            // TODO handling tick message loss not to keep inactive actors
            // TODO when started the talk with the broker cease sending ticks
            //      if timeout is reached, immediately request broker restart (?)
            //      or try with a different broker
            .match(msgInit.class, message -> {
                myBroker = message.broker;
                codes = message.codes;
                nextTick();
            })
            .match(CustomerSupervisor.msgConn.class, message -> {
                //printerActor.tell(new SystemSupervisor.msgLog(getSelf().path().name() + " Received connectivity list of "+message.conn.size()),getSelf());
                conn = message.conn;
            })
            .matchEquals("tick", message -> {
                // place the next order or retry one from backlog
                String orderNo = self().path().name()+"_"+orderCounter;
                Broker.msgSearch mySearch;

                // new order
                // TODO random MIN and MAX time more or less within schedule period
                //   (arrival can occur in the next period)
                //   departure/arrival
                // TODO random relation based on "common knowledge" + some other searches

                String relation = "";

                // choose a flight from any A to any B
                Boolean fullyRandom = true;
                // fullyRandom for some part of queries
                if(rndMode.nextInt(100) % 10 != 0)
                    fullyRandom = conn.isEmpty();

                if(!fullyRandom) // choose from the propagated connectivity map
                    relation = conn.get(rndRelation.nextInt(conn.size()));
                else {
                    int from = rndRelation.nextInt(codes.size());
                    int to = rndRelation.nextInt(codes.size());
                    if(from == to)
                        to = (to + 1) % codes.size();
                    relation = codes.get(from) + "-" + codes.get(to);
                }

                mySearch = new Broker.msgSearch(orderNo, relation, 2, "first", getSelf());
                printerActor.tell(
                        new SystemSupervisor.msgLog(
                                self().path().name()+ " is looking for a ticket" + (fullyRandom ? "*** " : " ") +
                                        mySearch.relation + " via "+myBroker.path().name()),
                        getSelf());

                myOrders.put(orderNo,mySearch);
                orderCounter++;

                myBroker.tell(mySearch,getSelf());
                // TODO pause ticking, resume when received an offer or a timeout has occurred
                // send another periodic tick after the specified delay
                // schedule the next query
                nextTick();
            })
            .match(Airline.airlineOffer.class, message -> {
                if(message.flightNo == -1) {
                    printerActor.tell(new SystemSupervisor.msgLog(getSelf().path().name() +
                        " failed search for order " + message.orderNo + ", " +
                        message.relation+" via " + sender().path().name()), getSelf());
                    //backlog.put(message.orderNo, myOrders.get(message.orderNo));
                } else { // broker tells he has bought something
                    printerActor.tell(new SystemSupervisor.msgLog(getSelf().path().name() +
                        " received an offer for order " + message.orderNo + ": " +
                        message.relation + ", from " + sender().path().name()), getSelf());
                }
                // pay for the offer
                sender().tell(new Broker.msgPayment(message.orderNo, message.flightNo), getSelf());
            })
            .match(Airline.msgConfirmPayment.class, message -> {
                if(myOrders.containsKey(message.offer.orderNo)) {
                    // check if the tickets bought by the broker match the order
                    if(verifyOrder(myOrders.get(message.offer.orderNo), message.offer)) {
                        printerActor.tell(new SystemSupervisor.msgLog(getSelf().path().name() +
                            " successfully bought a ticket " + message.offer.orderNo + ", " +
                            message.offer.airline.path().name() + ", " +
                            message.offer.relation + " via " + sender().path().name()), getSelf());
                    } else
                        printerActor.tell(new SystemSupervisor.msgLog(getSelf().path().name() +
                            " ERROR IN TICKET " + message.offer.orderNo + ", " +
                            message.offer.airline.path().name() + ", " +
                            message.offer.relation + " via " + sender().path().name()), getSelf());

                    //backlog.remove(message.offer.orderNo);
                } else { // should never happen
                    printerActor.tell(new SystemSupervisor.msgLog(getSelf().path().name() +
                        " received a response to an unrecognized order #" + message.offer.orderNo +
                        " from " + message.offer.airline.path().name()), getSelf());
                }
            })
            .matchEquals("restart", message -> {
                throw new ArithmeticException();
            })
            .build();
    }

    public Boolean verifyOrder(Broker.msgSearch search, Airline.airlineOffer offer) {
        if(search.relation != offer.relation)
            return false;
        if(search.seats != offer.seatIndices.length)
            return false;
        return true;
    }

    private void nextTick() {
        long tickDelay = Math.abs((long)((rnd.nextGaussian() * 3000.0) + 4000.0));
        tick = getContext().getSystem().scheduler().scheduleOnce(
                Duration.create(tickDelay, TimeUnit.MILLISECONDS),
                getSelf(), "tick", getContext().dispatcher(), null);
    }

    Map<String,Broker.msgSearch> myOrders;
    List<String> conn;
    List<String> codes;
}

