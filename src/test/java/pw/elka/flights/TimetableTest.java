package pw.elka.flights;

import org.junit.Test;

import static org.junit.Assert.*;

public class TimetableTest {
    private WorldFacts wf;

    public TimetableTest() {
        wf = new WorldFacts("coordinates.txt");
    }
    @Test
    public void loadFile() {
        FlightCalendar c = new FlightCalendar();
        Timetable t = new Timetable(c.getMsPerWeek(), wf);

        t.loadFile("lot.txt");
    }
}