package pw.elka.flights;

import java.util.*;
import java.util.stream.Collectors;

public class TestAStarNode extends AStarNode {
    public float getCost(AStarNode node) {
        float val = (float)metrics.getDist(point + "-" + ((TestAStarNode)node).point);
        System.out.println("Get neighbour cost for "+point +"->"+((TestAStarNode) node).point+ " = "+val);
        return val;
    }
    public float getEstimatedCost(AStarNode node) {
        float val = (float)metrics.getDist(point + "-" + ((TestAStarNode)node).point);
        System.out.println("Get est. cost for "+point + " -> "+val);
        return val;
    }
    public List getNeighbors() {
        List<TestAStarNode> n = nodes.stream()
            .filter(node -> this.neighbors.contains(((TestAStarNode)node).point))
            .collect(Collectors.toList());
        System.out.println("Get "+this.neighbors.size()+" neighbours of  "+point +" -> "+n.size());
        n.forEach(a -> System.out.print(((TestAStarNode)a).point+", "));
        System.out.println("");
        neighbors.forEach(a -> System.out.print(a+", "));
        return n;
    }

    public TestAStarNode(AStarNode parent, String point, WorldFacts metrics, List<TestAStarNode>nodes) {
        System.out.println("Create node for "+point);
        this.pathParent = parent;
        this.point = point;
        this.metrics = metrics;
        this.neighbors = new LinkedList<>();
        this.nodes = nodes;
    }

    public String point;
    WorldFacts metrics;
    List<String> neighbors;
    List<TestAStarNode>nodes;
}