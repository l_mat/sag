package pw.elka.flights;

import static org.junit.Assert.assertEquals;


import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import akka.actor.ActorSystem;
import akka.testkit.javadsl.TestKit;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

public class SimpleFlightCalendarTest {
    static ActorSystem system;

    private WorldFacts wf;

    public SimpleFlightCalendarTest() {
        wf = new WorldFacts("coordinates.txt");
    }

    @BeforeClass
    public static void setup() {
        system = ActorSystem.create();
    }

    @AfterClass
    public static void teardown() {
        TestKit.shutdownActorSystem(system);
        system = null;
    }

    @Test
    public void SimpleTimetableTest() {

        Timetable tt = new Timetable(FlightCalendar.getMsPerWeek(),wf);
        FlightCalendar cal = new FlightCalendar();
        String relation = "WAW-BRI";
        Airplane plane = new Airplane(new Flight(relation,cal.getTime()+1600, 3,wf.getDist(relation)),cal.getTime()+1600,111);

        tt.add("WAW-BRI", 4000);
        tt.add("WAW-LTN", 2000);
        tt.add("WAW-OSL", 1000);
        tt.add("WAW-CDG", 3000);
        tt.add("WAW-CGN",1000);

        for(Flight i : tt.table) {
            System.out.println(i.getDeparture() + " " + i.getRelation());
        }

        Optional<Integer []> res = plane.assignSeats(5,"orderNo");
        if(res.isPresent())
            System.out.println(res.get().length);

        for(int k = 0; k<3;k++) {
            try {
                TimeUnit.SECONDS.sleep(1);
                long now = cal.getTime();
                System.out.println(now + " " + ((now >= plane.getDeparture()) ? "" : "jeszcze nie") +" odleciał");
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        Date d = cal.asDate();
        SimpleDateFormat dt = new SimpleDateFormat("E yyyy-MM-dd HH:mm:ss");
        System.out.println(dt.format(d));
        System.out.println(FlightCalendar.getString(FlightCalendar.getTime()));

        /*
        final TestKit testProbe = new TestKit(system);
        final ActorRef testCustomer = system.actorOf(Customer.props(testProbe.getRef()));
        final ActorRef testBroker = system.actorOf(Broker.props(testProbe.getRef()));
        testCustomer.tell(new msgInit("Akka"), ActorRef.noSender());
        testCustomer.tell(new Greet(), ActorRef.noSender());
        SystemSupervisor.msgLog greeting = testProbe.expectMsgClass(SystemSupervisor.msgLog.class);
        assertEquals("Hello, Akka", greeting.message);
        */
    }
}
