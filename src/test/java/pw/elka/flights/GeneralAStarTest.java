package pw.elka.flights;

import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;

public class GeneralAStarTest {
    @Test
    public void AStar() {
        AStarSearch as = new AStarSearch();
        WorldFacts wf = new WorldFacts("coordinates.txt");

        List<TestAStarNode> nodes = new ArrayList<>();
        Set<String> codes = wf.getCodes();
        String start = "MAD", end="OSL";
        for(String s : codes) {
            TestAStarNode node = new TestAStarNode(null, s,  wf,nodes);
            for(String a: codes) {
                if (a != s) { // for test purposes
                    if ((start.equals(s) && end.equals(a)) || (end.equals(a) && start.equals(s))) {
                        System.out.println("SKIP " + a + "=>"+s);
                    } else {
                        node.neighbors.add(a);
                    }
                }
            }
            nodes.add(node);
        }
        // get one and only 1 item
        Optional<TestAStarNode> node1 = nodes.stream()
                .filter(user -> user.point.equals(start))
                .collect(Collectors.reducing((a, b) -> null));
        Optional<TestAStarNode> node2 = nodes.stream()
                .filter(user -> user.point.equals(end))
                .collect(Collectors.reducing((a, b) -> null));

        TestAStarNode startNode = node1.get();
        TestAStarNode goalNode = node2.get();

        Optional<List> result = as.findPath(startNode,goalNode);

        if(result.isPresent()) {
            List path = result.get();
            path.add(0,startNode);
            System.out.println("Result size: " + result.get().size());
            List<String> s = new LinkedList<>();
            result.get().forEach(a -> {
                s.add(((TestAStarNode)a).point );
                System.out.print(((TestAStarNode)a).point + ", ");
            });

            double d = wf.getDist(String.join("-",s));
            System.out.println("Calculated distance = "+d);
        } else {
            System.out.println("No result.");
        }
        for(String s : new String[]{"MAD->WAW->OSL", "MAD->AMS->OSL","MAD->CGN->OSL","MAD->CDG->OSL"}) {
            double d = wf.getDist(String.join("-", s));
            System.out.println("Calculated distance "+s+" = " + d);
        }

    }
}
