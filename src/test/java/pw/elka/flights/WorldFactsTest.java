package pw.elka.flights;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

public class WorldFactsTest {
    @Test
    public void reverseDirectionSameResult() {
        WorldFacts wf = new WorldFacts("coordinates.txt");
        double dist1 = wf.getDist("CGN->MAD");
        double dist2 = wf.getDist("CGN->MAD");
        System.out.println("dist1 = "+dist1);
        System.out.println("dist2 = "+dist2);
        assertEquals(dist1, dist2, 0.00001);
    }
    @Test
    public void anySeparators() {
        WorldFacts wf = new WorldFacts("coordinates.txt");
        double dist1 = wf.getDist("TIA->BRI");
        double dist2 = wf.getDist("TIA ,-BRI !!");
        System.out.println("dist1 = "+dist1);
        System.out.println("dist2 = "+dist2);
        assertEquals(dist1, dist2, 0.00001);
    }
    @Test
    public void chainedPath() {
        WorldFacts wf = new WorldFacts("coordinates.txt");
        double dist1 = wf.getDist("WAW->BRI->ZAG");
        double dist2 = wf.getDist("WAW->BRI");
        dist2 += wf.getDist("BRI->ZAG");
        System.out.println("dist1 = "+dist1);
        System.out.println("dist2 = "+dist2);
        assertEquals(dist1, dist2, 0.00001);
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();
    @Test
    public void invalidCode() {
        WorldFacts wf = new WorldFacts("coordinates.txt");
        thrown.expect(IndexOutOfBoundsException.class);
        thrown.expectMessage("Incorrect airport code");
        wf.getDist("WAW->INVALID->ZAG");
    }
    @Test
    public void invalidQuery() {
        WorldFacts wf = new WorldFacts("coordinates.txt");
        thrown.expect(IndexOutOfBoundsException.class);
        thrown.expectMessage("format");
        wf.getDist("WAW->");
    }
}